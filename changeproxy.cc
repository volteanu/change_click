#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/args.hh>
#include <click/packet.hh>
#include <clicknet/ip.h>
#include <clicknet/tcp.h>

#include "changeproxy.hh"

CLICK_DECLS

namespace Change
{
class HeaderRewriter
{
private:
	class Checksum
	{
	private:
		union uint_16_32_t
		{
			uint16_t shorts[2];
			uint32_t longs[1];
		};
		
		/* checksum and delta in host byte order */
		uint_16_32_t hbSum;
		
	public:
		inline Checksum()
		{
			hbSum.longs[0] = 0;
		}
		
		inline Checksum(uint16_t sum)
		{
			hbSum.longs[0] = ntohs(~sum);
		}
		
		inline void update16(uint16_t oldField, uint16_t newField)
		{
			uint16_t hbNew = ntohs(newField);
			uint16_t hbOld = ntohs(oldField);
			
			hbSum.longs[0] += hbNew;
			if (hbSum.longs[0] < hbOld)
				hbSum.longs[0] += 0xffff;
			hbSum.longs[0] -= hbOld;
		}
		
		inline void update32(uint32_t oldField, uint32_t newField)
		{
			uint_16_32_t oldFieldU;
			uint_16_32_t newFieldU;
			
			oldFieldU.longs[0] = oldField;
			newFieldU.longs[0] = newField;
			
			update16(oldFieldU.shorts[0], newFieldU.shorts[0]);
			update16(oldFieldU.shorts[1], newFieldU.shorts[1]);
		}
		
		inline void metaUpdate(uint16_t oldChecksum, uint16_t newChecksum)
		{
			update16(~oldChecksum, ~newChecksum);
		}
		
		uint16_t get();
	};
	
public:
	static void rewrite(click_ip *ipHeader, IPAddress src, IPAddress dst);
	
	static void rewriteTCP(click_ip *ipHeader, click_tcp *tcpHeader, IPAddress src, IPAddress dst);
};

uint16_t HeaderRewriter::Checksum::get()
{
	while (hbSum.longs[0] > 0xffff)
		hbSum.longs[0] = hbSum.shorts[0] + hbSum.shorts[1];
	
	return htons(~hbSum.longs[0]);
}

void HeaderRewriter::rewrite(click_ip *ipHeader, IPAddress src, IPAddress dst)
{
	Checksum checksum(ipHeader->ip_sum);
	
	/* rewrite the IP */
	if (src.addr())
	{
		uint32_t oldIp = ipHeader->ip_src.s_addr;
		
		ipHeader->ip_src.s_addr = src.addr();
		checksum.update32(oldIp, src.addr());
	}
	
	if (dst.addr())
	{
		uint32_t oldIp = ipHeader->ip_dst.s_addr;
		
		ipHeader->ip_dst.s_addr = dst.addr();
		checksum.update32(oldIp, dst.addr());
	}
	
	/* compute the checksum */
	ipHeader->ip_sum = checksum.get();
}

void HeaderRewriter::rewriteTCP(click_ip *ipHeader, click_tcp *tcpHeader, IPAddress src, IPAddress dst)
{
	Checksum tcpChecksum(tcpHeader->th_sum);
	uint16_t oldIpSum;
	uint16_t oldPort;
	
	/* rewrite the ip header */
	oldIpSum = ipHeader->ip_sum;
	rewrite(ipHeader, src, dst);
	
	/* update the TCP checksum */
	tcpChecksum.metaUpdate(oldIpSum, ipHeader->ip_sum); /* all changes in the IP header except for the checksum */
	tcpHeader->th_sum = tcpChecksum.get();
}

}

using namespace Change;

int ChangeProxy::configure(Vector<String> &conf, ErrorHandler *errh)
{
	if (Args(conf, this, errh)
		.read_mp("SRC", src)
		.read_mp("DST", dst)
		.complete() < 0)
	{
		return -1;
	}

	return 0;
}

void ChangeProxy::push(int port, Packet *p)
{
	WritablePacket *wp = p->uniqueify();
	
	if (port == P_TCP)
		HeaderRewriter::rewriteTCP(wp->ip_header(), wp->tcp_header(), src, dst);
	else
		HeaderRewriter::rewrite(wp->ip_header(),src, dst);
	
	output(0).push(wp);
}

CLICK_ENDDECLS

EXPORT_ELEMENT(ChangeProxy)

