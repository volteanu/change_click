#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/ipaddress.hh>
#include <click/args.hh>
#include <clicknet/ip.h>
#include <clicknet/tcp.h>
#include <clicknet/ether.h>
#include "../clickityclack/external/freebsdbob.hh"

#include "makebeforebreak.hh"

using namespace Change;

CLICK_DECLS

MakeBeforeBreak::MakeBeforeBreak()
	: timer(timerHook, this), reset(false) {}

MakeBeforeBreak::~MakeBeforeBreak() {}

int MakeBeforeBreak::configure(Vector<String> &conf, ErrorHandler *errh)
{
	if (Args(conf, this, errh)
		.read("PEER", peer)
		.read("RESET", reset)
		.complete() < 0)
	{
		return -1;
	}
	
	return 0;
}

int MakeBeforeBreak::initialize(ErrorHandler *errh)
{
	timer.initialize(this);
	timer.schedule_after_sec(TIMER_INTERVAL);

	return 0;
}

void MakeBeforeBreak::timerHook(Timer *timer, void *userData)
{
	MakeBeforeBreak *me = static_cast<MakeBeforeBreak *>(userData);

	me->states.checkExpiration(click_jiffies());
	
	timer->schedule_after_sec(TIMER_INTERVAL);
}

static inline size_t payloadLength(const click_ip *iph, const click_tcp *tcph)
{
	return ntohs(iph->ip_len) - iph->ip_hl * 4 - tcph->th_off * 4;
}

void MakeBeforeBreak::push(int port, Packet *p)
{
	PacketType type = (PacketType)port;
	
	if (type == PT_OUTBOUND)
	{
		const click_ip *iph = p->ip_header();
		const click_tcp *tcph = p->tcp_header();
		uint32_t seq = htonl(ntohl(tcph->th_seq) + payloadLength(iph, tcph));
		uint32_t ack = tcph->th_ack;
		uint16_t win = tcph->th_win;
		
		FiveTuple key(iph, tcph);
		MBBState *state = states.get(key);
		
		if (!state)
		{
			if (!(tcph->th_flags & TH_ACK))
				goto done;
			
			state = states.allocate(); assert(state);
			new(state) MBBState(key, seq, ack, win);
			states.put(state);
		}
		else
		{
			state->seq = seq;
			if (tcph->th_flags & TH_ACK)
				state->ack = ack;
			state->win = win;
			states.refresh(state);
		}
	}
	
done:
	output(port).push(p);
}

enum
{
	/* write handlers */
	H_SET_PEER,
};

void MakeBeforeBreak::add_handlers()
{
	add_write_handler("set_peer", &writeHandler, H_SET_PEER);
}

int MakeBeforeBreak::writeHandler(const String &conf, Element *e, void *thunk, ErrorHandler *errh)
{
	MakeBeforeBreak *me = static_cast<MakeBeforeBreak *>(e);
	
	switch ((intptr_t)thunk)
	{
	case H_SET_PEER:
		if (!IPAddressArg::parse(conf, me->peer))
			return errh->error("bad IP address");
		break;
		
	default:
		return errh->error("bad operation");
	}
	
end:
	return 0;
}

struct AddOption
{
	uint8_t kind;
	uint8_t len;
	uint8_t ipver:4,
		subtype:4; /* assume little endian */
	uint8_t addrid;
	uint32_t address; /* variable */
	
	AddOption()
		: kind(30), len(sizeof(AddOption)), ipver(4), subtype(3) {}
	
	void prime(uint32_t address)
	{
		this->address = address;
		addrid = ClickityClack::freeBSDBob(address);
	}
} __attribute__((__packed__));

struct NoOption
{
	uint8_t kind;
	
	NoOption()
		: kind(1) {}
}__attribute__((__packed__));

struct AddPacketData
{
	click_ip iph;
	click_tcp tcph;
	AddOption option;
	
	AddPacketData()
	{
		iph.ip_v = 4;
		iph.ip_hl = 5;
		iph.ip_tos = 0;
		iph.ip_len = htons(sizeof(AddPacketData));
		iph.ip_id = 0;
		iph.ip_off = 0;
		iph.ip_ttl = 64;
		iph.ip_p = IPPROTO_TCP;
		//iph.ip_sum;
		//iph.ip_src;
		//iph.ip_dst;
		
		//tcph.th_sport;
	        //tcph.th_dport;
	        //tcph.th_seq;
	        //tcph.th_ack;
		tcph.th_flags2 = 0;
		tcph.th_off = (sizeof(tcph) + sizeof(option)) / 4;
	        tcph.th_flags = TH_ACK;
	        //tcph.th_win
	        //tcph.th_sum;
	        tcph.th_urp = 0;
	}
	
	void prime(FiveTuple tuple, uint32_t seq, uint32_t ack, uint16_t win, uint32_t newIP)
	{
		iph.ip_src.s_addr = tuple.srcIP;
		iph.ip_dst.s_addr = tuple.dstIP;
		
		tcph.th_sport = tuple.srcPort;
		tcph.th_dport = tuple.dstPort;
		tcph.th_seq = seq;
		tcph.th_ack = ack;
		tcph.th_win = win;
		
		option.prime(newIP);
	}
} __attribute__((__packed__));

struct RstPacketData
{
	click_ip iph;
	click_tcp tcph;
	
	RstPacketData()
	{
		iph.ip_v = 4;
		iph.ip_hl = 5;
		iph.ip_tos = 0;
		iph.ip_len = htons(sizeof(RstPacketData));
		iph.ip_id = 0;
		iph.ip_off = 0;
		iph.ip_ttl = 64;
		iph.ip_p = IPPROTO_TCP;
		//iph.ip_sum;
		//iph.ip_src;
		//iph.ip_dst;
		
		//tcph.th_sport;
	        //tcph.th_dport;
	        //tcph.th_seq;
	        //tcph.th_ack;
		tcph.th_flags2 = 0;
		tcph.th_off = sizeof(tcph) / 4;
	        tcph.th_flags = TH_RST;
	        //tcph.th_win
	        //tcph.th_sum;
	        tcph.th_urp = 0;
	}
	
	void prime(FiveTuple tuple, uint32_t seq, uint32_t ack, uint16_t win)
	{
		iph.ip_src.s_addr = tuple.srcIP;
		iph.ip_dst.s_addr = tuple.dstIP;
		
		tcph.th_sport = tuple.srcPort;
		tcph.th_dport = tuple.dstPort;
		tcph.th_seq = seq;
		tcph.th_ack = ack;
		tcph.th_win = win;
	}
} __attribute__((__packed__));

int MakeBeforeBreak::suspend(ErrorHandler *)
{
	AddPacketData packetData;
	RstPacketData rstData;
	
	for (StateTrack<MBBState>::const_iterator it = states.begin(); it != states.end(); ++it)
	{
		const MBBState *state = (const MBBState *)it.get();
		Packet *packet;
		
		/* ACK */
		packetData.prime(state->key, state->seq, state->ack, state->win, peer.addr());
		packet = Packet::make(sizeof(click_ether), &packetData, sizeof(packetData), 0); assert(packet);
		output(2).push(packet);
		
		if (reset)
		{
			/* client-bound RST */
			rstData.prime(state->key, state->seq, state->ack, state->win);
			packet = Packet::make(sizeof(click_ether), &rstData, sizeof(rstData), 0); assert(packet);
			output(2).push(packet);
			
			/* server-bound RST */
			rstData.prime(state->key.reverse(), state->ack, state->seq, state->win);
			packet = Packet::make(sizeof(click_ether), &rstData, sizeof(rstData), 0); assert(packet);
			output(3).push(packet);
		}
	}
	
	states.clear();
	
	return 0;
}

CLICK_ENDDECLS

EXPORT_ELEMENT(MakeBeforeBreak)
