#ifndef CLICK_MAKEBEFOREBREAK_HH
#define CLICK_MAKEBEFOREBREAK_HH

#include <click/config.h>
#include <click/element.hh>
#include <click/timer.hh>
#include "lib/statetrack.hh"
#include "lib/fivetuple.hh"

CLICK_DECLS

class MakeBeforeBreak: public Element
{
private:
	enum PacketType
	{
		PT_OUTBOUND = 0,
		PT_INBOUND  = 1,
	};
	
	struct MBBState: public Change::State<Change::FiveTuple>
	{
		uint32_t seq;
		uint32_t ack;
		uint16_t win;
		
		MBBState(Change::FiveTuple key, uint32_t seq, uint32_t ack, uint16_t win)
			: Change::State<Change::FiveTuple>(key), seq(seq), ack(ack), win(win) {}
	};

	static const int TIMER_INTERVAL = 1;
	
	Change::StateTrack<MBBState> states;
	Timer timer;
	
	IPAddress peer;
	bool reset;
	
public:
	MakeBeforeBreak();
	
	~MakeBeforeBreak();
	
	const char *class_name() const { return "MakeBeforeBreak"; }
	
	const char *port_count() const { return "2/4"; }
	
	const char *processing() const { return PUSH; }
	
	int configure(Vector<String> &conf, ErrorHandler *errh);
	
	void push(int port, Packet *p);
	
	int initialize(ErrorHandler *errh);

	static void timerHook(Timer *timer, void *userData);
	
	static int writeHandler(const String &conf, Element *e, void *thunk, ErrorHandler *errh);
	
	//static String readHandler(Element *e, void *thunk);
	
	void add_handlers();
	
	int suspend(ErrorHandler *);
	
	//int resume(ErrorHandler *);
};

CLICK_ENDDECLS

#endif /* CLICK_MAKEBEFOREBREAK_HH */
