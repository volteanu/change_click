#ifndef CLICK_ALTER_HH
#define CLICK_ALTER_HH

#include <click/config.h>
#include <click/element.hh>

CLICK_DECLS

class Alter: public Element
{
private:
	int rate;
	
public:
	Alter()
		: rate(0) {}
	
	~Alter() {}
	
	const char *class_name() const { return "Alter"; }
	
	const char *port_count() const { return "1/1"; }
	
	const char *processing() const { return AGNOSTIC; }
	
	int configure(Vector<String> &conf, ErrorHandler *errh);
	
	Packet *simple_action(Packet *p);
};

CLICK_ENDDECLS

#endif /* CLICK_ALTER_HH */
