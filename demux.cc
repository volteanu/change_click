#include <click/config.h>
#include <click/args.hh>
#include <click/error.hh>
#include "demux.hh"

CLICK_DECLS

void Demux::push(int port, Packet *p)
{
	(void)port;
	
	output(sel).push(p);
}

enum
{
	H_SET,
	H_GET,
};

int Demux::writeHandler(const String &conf, Element *e, void *thunk, ErrorHandler *errh)
{
	Demux *me = (Demux *)e;
	
	switch ((intptr_t)thunk)
	{
	case H_SET:
		if (!IntArg().parse(conf, me->sel))
			return errh->error("bad port");
		break;
		
	default:
		return errh->error("bad operation");
	}
	
	return 0;
}

String Demux::readHandle(Element *e, void *thunk)
{
	Demux *me = (Demux *)e;
	
	switch ((intptr_t)thunk)
	{
	case H_GET:
		return String(me->sel);
		
	default:
		return "<error: bad operation>";
	}
}

void Demux::add_handlers()
{
	add_read_handler( "get", &readHandle,   H_GET);
	add_write_handler("set", &writeHandler, H_SET);
}

CLICK_ENDDECLS

EXPORT_ELEMENT(Demux)
