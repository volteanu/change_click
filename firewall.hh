#ifndef CLICK_FIREWALL_HH
#define CLICK_FIREWALL_HH

#include <click/config.h>
#include <click/element.hh>
#include <click/timer.hh>
#include "lib/statetrack.hh"
#include "lib/fivetuple.hh"

CLICK_DECLS

class Firewall: public Element
{
private:
	enum PacketType
	{
		PT_OUTBOUND = 0,
		PT_INBOUND  = 1,
	};

	static const int TIMER_INTERVAL = 1;
	
	typedef Change::State<Change::FiveTuple> FirewallState;
	
	Change::StateTrack<FirewallState> states;
	Timer timer;
	
public:
	Firewall();
	
	~Firewall();
	
	const char *class_name() const { return "Firewall"; }
	
	const char *port_count() const { return "2/2"; }
	
	const char *processing() const { return PUSH; }
	
	int configure(Vector<String>&, ErrorHandler*);
	
	void push(int port, Packet *p);
	
	template <typename L4_HEADER> bool process(Packet *p, const click_ip *iph, const L4_HEADER *l4h, PacketType type);
	
	int initialize(ErrorHandler *errh);

	static void timerHook(Timer *timer, void *userData);
};

CLICK_ENDDECLS

#endif /* CLICK_FIREWALL_HH */
