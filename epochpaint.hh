#ifndef CLICK_EPOCHPAINT_HH
#define CLICK_EPOCHPAINT_HH

#include <click/config.h>
#include <click/element.hh>

CLICK_DECLS

class EpochPaint: public Element
{
private:
	int epoch;
	int maxEpoch;
	
public:
	EpochPaint()
		: epoch(0) {}
	
	~EpochPaint() {}
	
	const char *class_name() const { return "EpochPaint"; }
	
	const char *port_count() const { return "1/1"; }
	
	const char *processing() const { return AGNOSTIC; }
	
	int configure(Vector<String> &conf, ErrorHandler *errh);
	
	Packet *simple_action(Packet *p);
	
	int suspend(ErrorHandler *);
	
	//int resume(ErrorHandler *);
};

CLICK_ENDDECLS

#endif /* CLICK_EPOCHPAINT_HH */
