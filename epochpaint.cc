#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/args.hh>
#include <click/packet_anno.hh>
#include "epochpaint.hh"

int EpochPaint::configure(Vector<String> &conf, ErrorHandler *errh)
{
	if (Args(conf, this, errh)
		.read_m("MAX_EPOCH", BoundedIntArg(1, 256), maxEpoch)
		.complete() < 0)
	{
		return -1;
	}
	
	return 0;
}

Packet *EpochPaint::simple_action(Packet *p)
{
	SET_PAINT_ANNO(p, epoch);
	return p;
}

int EpochPaint::suspend(ErrorHandler *)
{
	epoch = (epoch + 1) % maxEpoch;
}

EXPORT_ELEMENT(EpochPaint)
