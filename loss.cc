#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/args.hh>
#include <click/packet_anno.hh>
#include "loss.hh"

int Loss::configure(Vector<String> &conf, ErrorHandler *errh)
{
	if (Args(conf, this, errh)
		.read_m("RATE", BoundedIntArg(0, 100), rate)
		.complete() < 0)
	{
		return -1;
	}
	
	return 0;
}

Packet *Loss::simple_action(Packet *p)
{
	if (click_random() % 100 < rate)
	{
		p->kill();
		return NULL;
	}
	
	return p;
}

EXPORT_ELEMENT(Loss)
