#ifndef CLICK_CHANGELSR_HH
#define CLICK_CHANGELSR_HH

#include <click/config.h>
#include <click/element.hh>
#include <click/ipaddress.hh>

CLICK_DECLS

class ChangeLSR: public Element
{
	struct LSR
	{
		uint8_t type;
		uint8_t len;
		uint8_t ptr;
		uint32_t ip1;
		uint32_t ip2;
		uint8_t eol;

		LSR()
			: type(IPOPT_LSRR), len(11), ptr(4), eol(0) {}
	} __attribute__((__packed__));

	LSR lsr;

public:
	ChangeLSR() {}
	
	~ChangeLSR() {}
	
	const char *class_name() const { return "ChangeLSR"; }
	
	const char *port_count() const { return PORTS_1_1; }

	const char *processing() const { return AGNOSTIC; }

	int configure(Vector<String>&, ErrorHandler*);

	Packet *simple_action(Packet *p);
};

CLICK_ENDDECLS

#endif /* CLICK_CHANGELSR_HH */
