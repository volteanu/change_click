#include <click/config.h>
#include <click/args.hh>
#include <click/error.hh>
#include "synratelimit.hh"
#include "../clickityclack/lib/checksumfixup.hh"

using namespace Change;

CLICK_DECLS

using namespace ClickityClack;

SynRateLimit::SynRateLimit()
	: timer(timerHook, this) {}

SynRateLimit::~SynRateLimit() {}

int SynRateLimit::configure(Vector<String> &conf, ErrorHandler *errh)
{

	if (Args(conf, this, errh)
		.read_m("RATE", IntArg(), rate)
		.complete() < 0)
	{
		return -1;
	}

	if (rate <= 0)
		return errh->error("bad rate");
	
	states.setExpiryPeriod(CLICK_HZ / rate);
	
	return 0;
}

int SynRateLimit::initialize(ErrorHandler *errh)
{
	timer.initialize(this);
	timer.schedule_after_sec(TIMER_INTERVAL);

	return 0;
}

void SynRateLimit::timerHook(Timer *timer, void *userData)
{
	SynRateLimit *me = static_cast<SynRateLimit *>(userData);

	me->states.checkExpiration(click_jiffies());
	
	timer->schedule_after_sec(TIMER_INTERVAL);
}

void SynRateLimit::push(int port, Packet *p)
{
	PacketType type = (PacketType)port;
	const click_ip *iph = p->ip_header();
	const click_tcp *tcph = p->tcp_header();
	SRLState *state;
	click_jiffies_t now;

	if (type == PT_OUTBOUND)
		goto forward;
	
	if (!(tcph->th_flags & TH_SYN) || (tcph->th_flags & TH_ACK))
		goto forward;
	
	state = states.get(iph->ip_src.s_addr);
	if (!state)
	{
		state = states.allocate(); assert(state);
		new(state) SRLState(iph->ip_src.s_addr);
		states.put(state);
		goto forward;
	}
	
	/* check if the state should have expired */
	now = click_jiffies();
	if (state->expiryTime < now)
	{
		states.refresh(state, now);
		goto forward;
	}
	
	goto drop;
	
	
forward:
	output(port).push(p);
	return;
	
drop:
	p->kill();
}

CLICK_ENDDECLS

EXPORT_ELEMENT(SynRateLimit)
