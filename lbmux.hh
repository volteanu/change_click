#ifndef CLICK_LBMUX_HH
#define CLICK_LBMUX_HH

#include <click/config.h>
#include <click/element.hh>
#include <click/timer.hh>
#include "lib/statetrack.hh"
#include "lib/endpoint.hh"
#include "lib/listnode.hh"

CLICK_DECLS

class LBMux: public Element
{
private:
	static const int TIMER_INTERVAL = 1;
	
	struct DIP;
	
	struct Mapping: public Change::State<Change::Endpoint>
	{
		DIP *dip;
		
		List_member<Mapping> dipListLink;
		
		Mapping(Change::Endpoint key, DIP *dip)
			: Change::State<Change::Endpoint>(key), dip(dip) {}
		
		~Mapping();
	};
	
	typedef Change::StateTrack<Mapping> StateTracker;
	
	struct DIP
	{
		uint32_t ip;
		List<Mapping, &Mapping::dipListLink> mappings;
		
		List_member<DIP> dipQueueLink;
		
		DIP(uint32_t ip)
			: ip(ip) {}
	};
	
	typedef HashTable<uint32_t, DIP *> DIPTable;
	
	struct VIP
	{
		uint32_t ip;
		List<DIP, &DIP::dipQueueLink> dipQueue;
		DIPTable dips;
		StateTracker stateTracker;
		
		VIP(uint32_t ip, size_t prealloc)
			: ip(ip)
		{
			stateTracker.preallocate(prealloc);
		}
		
		bool addDIP(uint32_t ip);
		
		bool removeDIP(uint32_t ip);
		
		DIP *allocDIP()
		{
			DIP *ret = dipQueue.front();
			
			if (!ret)
				return ret;
			
			dipQueue.pop_front();
			dipQueue.push_back(ret);
			
			return ret;
		}
	};
	
	typedef HashTable<uint32_t, VIP *> VIPTable;
	VIPTable vipTable;
	Timer timer;
	int prealloc;
	
public:
	LBMux();
	
	~LBMux();
	
	const char *class_name() const { return "LBMux"; }
	
	const char *port_count() const { return "1/1"; }
	
	const char *processing() const { return AGNOSTIC; }
	
	int configure(Vector<String> &conf, ErrorHandler *errh);
	
	Packet *simple_action(Packet *p);
	
	int initialize(ErrorHandler *);

	static void timerHook(Timer *timer, void *userData);
	
	static int writeHandler(const String &conf, Element *e, void *thunk, ErrorHandler *errh);
	
	static String readHandle(Element *e, void *thunk);
	
	void add_handlers();
	
private:
	click_ip iph;
	
	WritablePacket *encapsulate(Packet *p, uint32_t vip, uint32_t dip);
};

CLICK_ENDDECLS

#endif /* CLICK_LBMUX_HH */
