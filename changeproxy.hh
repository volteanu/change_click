#ifndef CLICK_CHANGEPROXY_HH
#define CLICK_CHANGEPROXY_HH

#include <click/config.h>
#include <click/element.hh>
#include <click/ipaddress.hh>

CLICK_DECLS

class ChangeProxy: public Element
{
	enum Port
	{
		P_TCP     = 0,
		P_NON_TCP = 1,
	};

	IPAddress src;
	IPAddress dst;
	
public:
	ChangeProxy() {}
	
	~ChangeProxy() {}
	
	const char *class_name() const { return "ChangeProxy"; }
	
	const char *port_count() const { return "2/1"; }

	const char *processing() const { return PUSH; }

	int configure(Vector<String>&, ErrorHandler*);

	void push(int port, Packet *p);
};

CLICK_ENDDECLS

#endif /* CLICK_CHANGEPROXY_HH */
