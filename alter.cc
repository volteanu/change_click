#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/args.hh>
#include <click/packet_anno.hh>
#include <clicknet/ip.h>
#include <clicknet/tcp.h>
#include <clicknet/udp.h>
#include <clicknet/icmp.h>
#include "alter.hh"

int Alter::configure(Vector<String> &conf, ErrorHandler *errh)
{
	if (Args(conf, this, errh)
		.read_m("RATE", BoundedIntArg(0, 100), rate)
		.complete() < 0)
	{
		return -1;
	}
	
	return 0;
}

static void fuckUp(unsigned char *buf, size_t len)
{
	size_t byteIndex = click_random() % len;
	size_t bitIndex = click_random() % 8;
	unsigned char fuckz0r = 1 << bitIndex;
	
	buf[byteIndex] ^= fuckz0r;
}

Packet *Alter::simple_action(Packet *p)
{
	if (click_random() % 100 < rate && p->has_transport_header())
	{
		WritablePacket *wp = p->uniqueify();
		
		size_t transportHeaderSize;
	
		switch (p->ip_header()->ip_p)
		{
		case IPPROTO_TCP:
			if (p->transport_length() < (int)sizeof(click_tcp))
				goto fallback;
			transportHeaderSize = p->tcp_header()->th_off * 5;
			if (p->transport_length() < (int)transportHeaderSize)
				goto fallback;
			break;
	
		case IPPROTO_UDP:
			if (p->transport_length() < (int)sizeof(click_udp))
				goto fallback;
			transportHeaderSize = sizeof(click_udp);
			break;
	
		case IPPROTO_ICMP:
			if (p->transport_length() < (int)sizeof(click_icmp))
				goto fallback;
			transportHeaderSize = sizeof(click_icmp);
			break;
	
		default:
			goto fallback;
		}
		
		if (transportHeaderSize == 0)
			goto fallback;
		
		fuckUp(wp->transport_header() + transportHeaderSize, wp->transport_length() - transportHeaderSize);
				
		return wp;
	}
	
fallback:
	return p;
}

EXPORT_ELEMENT(Alter)
