#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/nameinfo.hh>
#include <clicknet/ip.h>
#include <clicknet/icmp.h>
#include <click/ipaddress.hh>
#include <click/vector.hh>

#include "changeenforcer.hh"

CLICK_DECLS

using namespace Change;

enum
{
	/* write handlers */
	H_DELEGATED_ADD,
	H_DELEGATED_DEL,
	
	H_DELEGATED_ADD_PREFIX,
	H_DELEGATED_DEL_PREFIX,
	
	H_DELEGATED_ADD_RANGE,
	H_DELEGATED_DEL_RANGE,
	
	H_DELEGATED_CLEAR,
	
	H_DELEGATED_LIST,
};

ChangeEnforcer::ChangeEnforcer()
	: timer(timerHook, this) {}

ChangeEnforcer::~ChangeEnforcer() {}

int ChangeEnforcer::configure(Vector<String> &conf, ErrorHandler *errh)
{
	Vector<IPAddress> platformAddresses;
	Vector<IPAddress> explicitAddresses;
	int allocCount = 0;

	if (Args(conf, this, errh)
		.read_m("ADDR", IPAddressArg(), platformAddresses)
		.read("EXPLICIT", IPAddressArg(), explicitAddresses)
		.read_m("PREALLOC", IntArg(), allocCount)
		.complete() < 0)
	{
		return -1;
	}

	for (int i = 0; i < platformAddresses.size(); i++)
		platformIPs.add(platformAddresses[i].addr());
	
	for (int i = 0; i < explicitAddresses.size(); i++)
		explicitIPs.add(explicitAddresses[i].addr());

	implicitIPs.preallocate(allocCount);

	return 0;
}

int ChangeEnforcer::initialize(ErrorHandler *errh)
{
	timer.initialize(this);
	timer.schedule_after_sec(TIMER_INTERVAL);

	return 0;
}



void ChangeEnforcer::push(int port, Packet *packet)
{
	const click_ip *ipHeader = packet->ip_header();
	uint32_t src = ipHeader->ip_src.s_addr;
	uint32_t dst = ipHeader->ip_dst.s_addr;
	
//	if (port == PT_PM_BOUND)
//		click_chatter("PM BOUND");
//	else
//		click_chatter("INTERNET BOUND");
	
	/* case 0 */
	if (port == PT_PM_BOUND &&
		platformIPs.contains(dst))
	{
//		click_chatter("case 0");
		if (isExternal(src))
			implicitIPs.sighting(src);
		goto forward;
	}
		
	/* case 1 */
	if (port == PT_INTERNET_BOUND &&
		platformIPs.contains(src) &&
		implicitIPs.contains(dst))
	{
//		click_chatter("case 1");
		goto forward;
	}

	/* case 2a */
	if (port == PT_INTERNET_BOUND &&
		explicitIPs.contains(dst) &&
		implicitIPs.contains(src))
	{
//		click_chatter("case 2a");
		goto forward;
	}
	
	/* case 2b */
	if (port == PT_PM_BOUND &&
		explicitIPs.contains(dst) &&
		isExternal(src))
	{
//		click_chatter("case 2b");
		implicitIPs.sighting(src);
		goto forward;
	}

	/* case 3 */
	if (!isExternal(src) &&
		explicitIPs.contains(dst))
	{
//		click_chatter("case 3");
		goto forward;
	}

	/* none of the above */
//	click_chatter("drop");
	goto drop;


forward:
	output(port).push(packet);
	return;

drop:
	packet->kill();
	return;

do_nothing:
	/* ideally, we shouldn't use this */
	return;
}

int ChangeEnforcer::writeHandler(const String &conf, Element *e, void *thunk, ErrorHandler *errh)
{
	ChangeEnforcer *me = (ChangeEnforcer *)e;
	IPAddress address;
	IPAddress mask;
	Vector<IPAddress> addresses;
	IPSet::Range range;
	IPSet::Prefix prefix;
	
	switch ((intptr_t)thunk)
	{
	case H_DELEGATED_ADD:
	case H_DELEGATED_DEL:
		if (!IPAddressArg::parse(conf, address))
			return errh->error("bad IP address");
		break;
		
	case H_DELEGATED_ADD_RANGE:
	case H_DELEGATED_DEL_RANGE:
		if (!IPAddressArg::parse(conf, addresses) || addresses.size() != 2 || ntohl(addresses[0].addr()) > ntohl(addresses[1].addr()))
			return errh->error("bad IP range");
		range = IPSet::Range(addresses[0], addresses[1]);
		break;
		
	case H_DELEGATED_ADD_PREFIX:
	case H_DELEGATED_DEL_PREFIX:
		if (!IPPrefixArg().parse(conf, address, mask))
			return errh->error("bad IP prefix");
		prefix = IPSet::Prefix(address, mask);
		break;
		
	case H_DELEGATED_CLEAR:
		break;
		
	default:
		return errh->error("bad operation");
	}
	
	switch ((intptr_t)thunk)
	{
	case H_DELEGATED_ADD:
		me->explicitIPs.add(address.addr());
		return 0;
		
	case H_DELEGATED_DEL:
		me->explicitIPs.del(address.addr());
		return 0;
		
	case H_DELEGATED_ADD_RANGE:
		me->explicitIPs.add(range);
		return 0;
		
	case H_DELEGATED_DEL_RANGE:
		me->explicitIPs.del(range);
		break;
		
	case H_DELEGATED_ADD_PREFIX:
		me->explicitIPs.add(prefix);
		return 0;
		
	case H_DELEGATED_DEL_PREFIX:
		me->explicitIPs.del(prefix);
		break;
		
	case H_DELEGATED_CLEAR:
		me->explicitIPs.clear();
		return 0;
		
	default:
		return errh->error("bad operation");
	}
	
end:
	return 0;
}

String ChangeEnforcer::readHandle(Element *e, void *thunk)
{
	ChangeEnforcer *me = (ChangeEnforcer *)e;
	String ret;
	
	switch ((intptr_t)thunk)
	{
	case H_DELEGATED_LIST:
		for (HashTable<Change::HashInt<uint32_t>, bool>::iterator it = me->explicitIPs.ips.begin(); it != me->explicitIPs.ips.end(); it++)
			ret += IPAddress(it.key()).unparse() + "\n";
		for (int i = 0; i < me->explicitIPs.ranges.size(); i++)
			ret += me->explicitIPs.ranges[i].unparse() + "\n";
		for (int i = 0; i < me->explicitIPs.prefixes.size(); i++)
			ret += me->explicitIPs.prefixes[i].unparse() + "\n";
		return ret;
		
	default:
		return "<error: bad operation>";
	}
	
	return ret;
}

void ChangeEnforcer::add_handlers()
{
	add_read_handler( "list",       &readHandle,   H_DELEGATED_LIST);
	
	add_write_handler("add",        &writeHandler, H_DELEGATED_ADD);
	add_write_handler("del",        &writeHandler, H_DELEGATED_DEL);
	
	add_write_handler("add_prefix", &writeHandler, H_DELEGATED_ADD_PREFIX);
	add_write_handler("del_prefix", &writeHandler, H_DELEGATED_DEL_PREFIX);
	
	add_write_handler("add_range",  &writeHandler, H_DELEGATED_ADD_RANGE);
	add_write_handler("del_range",  &writeHandler, H_DELEGATED_DEL_RANGE);
	
	add_write_handler("clear",      &writeHandler, H_DELEGATED_CLEAR);
}

void ChangeEnforcer::timerHook(Timer *timer, void *userData)
{
	ChangeEnforcer *me = reinterpret_cast<ChangeEnforcer *>(userData);

	me->implicitIPs.checkExpiration();
	timer->schedule_after_sec(TIMER_INTERVAL);
}

CLICK_ENDDECLS

EXPORT_ELEMENT(ChangeEnforcer)
ELEMENT_REQUIRES(Change_IPSet)
