#ifndef CLICK_CHANGEENFORCER_HH
#define CLICK_CHANGEENFORCER_HH

#include <click/element.hh>
#include <click/hashtable.hh>
#include <click/timer.hh>
#include "lib/ipset.hh"
#include "lib/statetrack.hh"

CLICK_DECLS

class ChangeEnforcer: public Element
{
private:
	enum PacketType
	{
		PT_PM_BOUND       = 0,
		PT_INTERNET_BOUND = 1,
	};

	class IPTrack: public Change::StateTrack<Change::State<uint32_t> >
	{
		typedef Change::State<uint32_t> IPEntry;
		
	public:
		void sighting(uint32_t ip, click_jiffies_t now = click_jiffies())
		{
			IPEntry *entry = get(ip);
			
			if (!entry)
			{
				entry = allocate(); assert(entry);
				new(entry) IPEntry(ip);
				put(entry, now);
			}
			else
			{
				refresh(entry, now);
			}
		}
	};

	static const int TIMER_INTERVAL = 1;
	
	IPTrack implicitIPs;
	Change::IPSet platformIPs;
	Change::IPSet explicitIPs;
	Timer timer;
	
public:
	ChangeEnforcer();
	
	~ChangeEnforcer();
	
	const char *class_name() const { return "ChangeEnforcer"; }
	
	const char *port_count() const { return "2/2"; }
	
	const char *processing() const { return PUSH; }
	
	int configure(Vector<String>&, ErrorHandler*);
	
	void push(int, Packet *);
	
	int initialize(ErrorHandler *errh);
	
	static int writeHandler(const String &conf, Element *e, void *thunk, ErrorHandler *errh);
	
	static String readHandle(Element *e, void *thunk);
	
	void add_handlers();

	static void timerHook(Timer *timer, void *userData);

private:
	inline bool isExternal(uint32_t ip)
	{
		return !(explicitIPs.contains(ip) || platformIPs.contains(ip));
	}
};

CLICK_ENDDECLS

#endif /* CLICK_CHANGEENFORCER_HH */
