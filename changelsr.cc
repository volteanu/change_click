#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/args.hh>
#include <click/packet.hh>
#include <clicknet/ip.h>

#include "changelsr.hh"

CLICK_DECLS

int ChangeLSR::configure(Vector<String> &conf, ErrorHandler *errh)
{
	IPAddress addr;

	if (Args(conf, this, errh)
		.read_p("DST", addr)
		.complete() < 0)
	{
		return -1;
	}

	lsr.ip2 = addr.addr();

	return 0;
}

Packet *ChangeLSR::simple_action(Packet *p)
{
	click_ip ipHeader = *(reinterpret_cast<const click_ip *>(p->data()));
	WritablePacket *wp = p->push(sizeof(LSR));


	lsr.ip1 = ipHeader.ip_dst.s_addr;
	ipHeader.ip_dst.s_addr = lsr.ip2;
	ipHeader.ip_hl += sizeof(LSR) / 4;
	ipHeader.ip_len = htons(ntohs(ipHeader.ip_len) + sizeof(LSR));

	memcpy(wp->data(), &ipHeader, sizeof(ipHeader));
	memcpy(wp->data() + sizeof(ipHeader), &lsr, sizeof(lsr));
	wp->set_ip_header(reinterpret_cast<click_ip *>(wp->data()), ntohs(ipHeader.ip_len));

	return wp;
}

CLICK_ENDDECLS

EXPORT_ELEMENT(ChangeLSR)
