#ifndef CLICK_LOSS_HH
#define CLICK_LOSS_HH

#include <click/config.h>
#include <click/element.hh>

CLICK_DECLS

class Loss: public Element
{
private:
	int rate;
	
public:
	Loss()
		: rate(0) {}
	
	~Loss() {}
	
	const char *class_name() const { return "Loss"; }
	
	const char *port_count() const { return "1/1"; }
	
	const char *processing() const { return AGNOSTIC; }
	
	int configure(Vector<String> &conf, ErrorHandler *errh);
	
	Packet *simple_action(Packet *p);
};

CLICK_ENDDECLS

#endif /* CLICK_LOSS_HH */
