#ifndef CLICK_CHANGE_ENDPOINTID_HH
#define CLICK_CHANGE_ENDPOINTID_HH

#include <click/config.h>
#include <clicknet/ip.h>
#include <clicknet/tcp.h>
#include <clicknet/udp.h>
#include <clicknet/icmp.h>
#include <click/hashcode.hh>
#include "../../clickityclack/external/freebsdbob.hh"

CLICK_DECLS

namespace Change
{

/*
 * Unless otherwise stated, all parameters/fields are in network byte order.
 */

struct Endpoint
{
	uint32_t ip;
	uint16_t port;
	uint8_t proto;
	
	inline Endpoint() {}
	
	inline Endpoint(uint32_t ip, uint16_t port, uint8_t proto)
		: ip(ip), port(port), proto(proto) {}
	
	inline Endpoint(const click_ip *ipHeader, const click_tcp *tcpHeader, bool src)
		: proto(IPPROTO_TCP)
	{
		if (src)
		{
			ip = ipHeader->ip_src.s_addr;
			port = tcpHeader->th_sport;
		}
		else
		{
			ip = ipHeader->ip_dst.s_addr;
			port = tcpHeader->th_dport;
		}
	}
	
	inline Endpoint(const click_ip *ipHeader, const click_udp *udpHeader, bool src)
		: proto(IPPROTO_UDP)
	{
		if (src)
		{
			ip = ipHeader->ip_src.s_addr;
			port = udpHeader->uh_sport;
		}
		else
		{
			ip = ipHeader->ip_dst.s_addr;
			port = udpHeader->uh_dport;
		}
	}
	
	inline Endpoint(const click_ip *ipHeader, const click_icmp_sequenced *icmpHeader, bool src)
		: proto(IPPROTO_UDP)
	{
		if (src)
			ip = ipHeader->ip_src.s_addr;
		else
			ip = ipHeader->ip_dst.s_addr;
		port = icmpHeader->icmp_identifier;
	}
	
	Endpoint(const click_ip *ipHeader, const void *l4Header, bool src)
	{
		switch (ipHeader->ip_p)
		{
		case IPPROTO_TCP:
			*this = Endpoint(ipHeader, reinterpret_cast<const click_tcp *>(l4Header), src);
			break;
			
		case IPPROTO_UDP:
			*this = Endpoint(ipHeader, reinterpret_cast<const click_udp *>(l4Header), src);
			break;
			
		case IPPROTO_ICMP:
			*this = Endpoint(ipHeader, reinterpret_cast<const click_icmp_sequenced *>(l4Header), src);
			break;	
		}
	}
	
	hashcode_t hashcode() const
	{
		return ClickityClack::freeBSDBob(ip, port, proto);
	}
	
	bool operator ==(const Endpoint &other) const
	{
		if (this->port == other.port && this->ip == other.ip && this->proto == other.proto)
			return true;
		else
			return false;
	}
	
	inline bool operator !=(const Endpoint &other) const
	{
		if (this->proto != other.proto || this->port != other.port || this->ip != other.ip)
			return true;
		else
			return false;
	}
};

}

CLICK_ENDDECLS

#endif /* CLICK_CHANGE_ENDPOINTID_HH */
