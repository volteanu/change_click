#ifndef CLICK_CHANGE_LISTNODE_HH
#define CLICK_CHANGE_LISTNODE_HH

#include <click/config.h>
#include <click/list.hh>

CLICK_DECLS

namespace Change
{

template <typename T> struct ListNode
{
	T value;
	List_member<ListNode<T> > link;
	
	ListNode(T value)
		: value(value) {}
};

template <typename T> class MetaList: public List<ListNode<T>, &ListNode<T>::link>
{
public:
	typedef ListNode<T> Node;
};

}

CLICK_ENDDECLS

#endif /* CLICK_CHANGE_LISTNODE_HH */
