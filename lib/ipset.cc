#include <click/config.h>
#include "ipset.hh"

namespace Change
{

bool IPSet::contains(uint32_t ip)
{
	if (ips.find(ip) != ips.end())
		return true;

	/* commented out for better performance*/
	/*for (int i = 0; i < ranges.size(); i++)
	{
		if (ranges[i].match(ip))
			return true;
	}

	for (int i = 0; i < prefixes.size(); i++)
	{
		if (prefixes[i].match(ip))
			return true;
	}*/

	return false;
}

void IPSet::add(Range range)
{
	for (Vector<Range>::iterator it = ranges.begin(); it != ranges.end(); it++)
	{
		if (*it == range)
			return;
	}
	ranges.push_back(range);
}

void IPSet::add(Prefix prefix)
{
	for (Vector<Prefix>::iterator it = prefixes.begin(); it != prefixes.end(); it++)
	{
		if (*it == prefix)
			return;
	}
	prefixes.push_back(prefix);
}

void IPSet::del(Range range)
{
	for (Vector<Range>::iterator it = ranges.begin(); it != ranges.end(); it++)
	{
		if (*it == range)
		{
			ranges.erase(it);
			break;
		}
	}
}

void IPSet::del(Prefix prefix)
{
	for (Vector<Prefix>::iterator it = prefixes.begin(); it != prefixes.end(); it++)
	{
		if (*it == prefix)
		{
			prefixes.erase(it);
			break;
		}
	}
}

}

ELEMENT_PROVIDES(Change_IPSet)
