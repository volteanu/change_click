#ifndef CLICK_CHANGE_HASHINT_HH
#define CLICK_CHANGE_HASHINT_HH

#include <click/config.h>
#include <click/crc32.h>
#include <click/hashcode.hh>

CLICK_DECLS

namespace Change
{

template <typename T> struct HashInt
{
	T value;
	
	inline HashInt() {}
	
	inline HashInt(T value)
		: value(value) {}
	
	inline operator T() const
	{
		return value;
	}
	
	inline hashcode_t hashcode() const
	{
		return update_crc(0, (char *)&value, sizeof(value));
	}
};

}

CLICK_ENDDECLS

#endif /* CLICK_CHANGE_HASHINT_HH */
