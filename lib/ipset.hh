#ifndef CHANGE_IPSET_H
#define CHANGE_IPSET_H

#include <click/config.h>
#include <click/string.hh>
#include <click/ipaddress.hh>
#include <click/hashtable.hh>
#include <click/vector.hh>
#include "hashint.hh"

namespace Change
{

class IPSet
{
public:
	struct Prefix
	{
		uint32_t ip;
		uint32_t mask;

		Prefix() {}

		Prefix(uint32_t ip, uint32_t mask)
			: ip(ip & mask), mask(mask) {}

		Prefix(IPAddress ip, IPAddress mask)
			: ip(ip.addr() & mask.addr()), mask(mask.addr()) {}

		bool match(uint32_t ip)
		{
			return ip & mask == this->ip;
		}

		String unparse()
		{
			return IPAddress(ip).unparse_with_mask(mask);
		}

		bool operator ==(const Prefix &other) const
		{
			return ip == other.ip && mask == other.mask;
		}
	};

	struct Range
	{
		uint32_t hbStart;
		uint32_t hbEnd;

		Range() {}

		Range(uint32_t start, uint32_t end)
			: hbStart(ntohl(start)), hbEnd(ntohl(end)) {}

		Range(IPAddress start, IPAddress end)
			: hbStart(ntohl(start.addr())), hbEnd(ntohl(end.addr())) {}

		bool match(uint32_t ip)
		{
			uint32_t hbIp = ntohl(ip);

			return hbIp >= hbStart && hbIp <= hbEnd;
		}

		String unparse()
		{
			return IPAddress(htonl(hbStart)).unparse() + "-" + IPAddress(htonl(hbEnd)).unparse();
		}

		bool operator ==(const Range &other) const
		{
			return hbStart == other.hbStart && hbEnd == other.hbEnd;
		}
	};

	HashTable<Change::HashInt<uint32_t>, bool> ips;
	Vector<Prefix> prefixes;
	Vector<Range> ranges;

	bool contains(uint32_t ip);

	void add(uint32_t ip)
	{
		ips.set(ip, true);
	}

	void add(Range range);

	void add(Prefix prefix);

	void del(uint32_t ip)
	{
		ips.erase(ip);
	}

	void del(Range range);

	void del(Prefix prefix);

	void clear()
	{
		ips.clear();
		ranges.clear();
		prefixes.clear();
	}
};

}

#endif /* CHANGE_IPSET_H */
