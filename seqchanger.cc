#include "seqchanger.hh"
#include "../clickityclack/lib/checksumfixup.hh"

using namespace Change;

CLICK_DECLS

using namespace ClickityClack;

SEQChanger::SEQChanger()
	: timer(timerHook, this) {}

SEQChanger::~SEQChanger() {}

int SEQChanger::configure(Vector<String> &, ErrorHandler *)
{
	/* TODO? */
	return 0;
}

int SEQChanger::initialize(ErrorHandler *errh)
{
	timer.initialize(this);
	timer.schedule_after_sec(TIMER_INTERVAL);

	return 0;
}

void SEQChanger::timerHook(Timer *timer, void *userData)
{
	SEQChanger *me = static_cast<SEQChanger *>(userData);

	me->states.checkExpiration(click_jiffies());
	
	timer->schedule_after_sec(TIMER_INTERVAL);
}

bool SEQChanger::processOutbound(click_ip *iph, click_tcp *tcph)
{
	FiveTuple key(iph, tcph);
	SEQState *state = states.get(key);
	uint32_t oldSeq;
	
	if (!state)
	{
		uint32_t delta;
		
		/* don't wanna ruin existing flows */
		if (!(tcph->th_flags & TH_SYN) || (tcph->th_flags & TH_ACK))
			return true;
		
		delta = (uint32_t)click_random() - tcph->th_seq;
		
		state = states.allocate(); assert(state);
		new(state) SEQState(key, delta);
		states.put(state);
	}
	else
	{
		states.refresh(state);
	}
	
	oldSeq = tcph->th_seq;
	tcph->th_seq += state->delta;
	tcph->th_sum = checksumFold(checksumFixup32(oldSeq, tcph->th_seq, tcph->th_sum));
	
	return true;
}

bool SEQChanger::processInbound(click_ip *iph, click_tcp *tcph)
{
	if (!(tcph->th_flags & TH_ACK))
		return true;
	
	FiveTuple key = FiveTuple(iph, tcph).reverse();
	SEQState *state = states.get(key);
	uint32_t oldSeq;
	
	if (!state)
		return true;
	
	oldSeq = tcph->th_ack;
	tcph->th_ack -= state->delta;
	tcph->th_sum = checksumFold(checksumFixup32(oldSeq, tcph->th_ack, tcph->th_sum));
	
	return true;
}

void SEQChanger::push(int port, Packet *p)
{
	WritablePacket *wp = p->uniqueify();
	PacketType type = (PacketType)port;
	click_ip *iph = wp->ip_header();
	click_tcp *tcph = wp->tcp_header();
	bool result = (type == PT_OUTBOUND) ? processOutbound(iph, tcph) : processInbound(iph, tcph);
	
	if (result)	
		output(port).push(p);
	else
		p->kill();
}

CLICK_ENDDECLS

EXPORT_ELEMENT(SEQChanger)
