#ifndef CLICK_SEQCHANGER_HH
#define CLICK_SEQCHANGER_HH

#include <click/config.h>
#include <click/element.hh>
#include <click/timer.hh>
#include "lib/statetrack.hh"
#include "lib/fivetuple.hh"

CLICK_DECLS

class SEQChanger: public Element
{
private:
	enum PacketType
	{
		PT_OUTBOUND = 0,
		PT_INBOUND  = 1,
	};
	
	struct SEQState: public Change::State<Change::FiveTuple>
	{
		uint32_t delta;
		
		SEQState(Change::FiveTuple key, uint32_t delta)
			: Change::State<Change::FiveTuple>(key), delta(delta) {}
	};

	static const int TIMER_INTERVAL = 1;
	
	Change::StateTrack<SEQState> states;
	Timer timer;
	
public:
	SEQChanger();
	
	~SEQChanger();
	
	const char *class_name() const { return "SEQChanger"; }
	
	const char *port_count() const { return "2/2"; }
	
	const char *processing() const { return PUSH; }
	
	int configure(Vector<String>&, ErrorHandler*);
	
	void push(int port, Packet *p);
	
	int initialize(ErrorHandler *errh);

	static void timerHook(Timer *timer, void *userData);
	
private:
	bool processOutbound(click_ip *iph, click_tcp *tcph);
	
	bool processInbound(click_ip *iph, click_tcp *tcph);
};

CLICK_ENDDECLS

#endif /* CLICK_SEQCHANGER_HH */
