#ifndef CLICK_SYNRATELIMIT_HH
#define CLICK_SYNRATELIMIT_HH

#include <click/config.h>
#include <click/element.hh>
#include <click/timer.hh>
#include "lib/statetrack.hh"
#include "lib/fivetuple.hh"

CLICK_DECLS

class SynRateLimit: public Element
{
private:
	enum PacketType
	{
		PT_OUTBOUND = 0,
		PT_INBOUND  = 1,
	};

	static const int TIMER_INTERVAL = 1;

	typedef Change::State<uint32_t> SRLState;

	Change::StateTrack<SRLState> states;
	Timer timer;
	int rate;
	
public:
	SynRateLimit();

	~SynRateLimit();
	
	const char *class_name() const { return "SynRateLimit"; }
	
	const char *port_count() const { return "2/2"; }
	
	const char *processing() const
	{
		return PUSH;
	}
	
	int configure(Vector<String> &conf, ErrorHandler *errh);
	
	void push(int port, Packet *p);
	
	int initialize(ErrorHandler *errh);

	static void timerHook(Timer *timer, void *userData);
};

CLICK_ENDDECLS

#endif /* CLICK_SYNRATELIMIT_HH */
