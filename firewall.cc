#include "firewall.hh"

using namespace Change;

CLICK_DECLS

Firewall::Firewall()
	: timer(timerHook, this) {}

Firewall::~Firewall() {}

int Firewall::configure(Vector<String> &, ErrorHandler *)
{
	/* TODO? */
	return 0;
}

int Firewall::initialize(ErrorHandler *errh)
{
	timer.initialize(this);
	timer.schedule_after_sec(TIMER_INTERVAL);

	return 0;
}

void Firewall::timerHook(Timer *timer, void *userData)
{
	Firewall *me = static_cast<Firewall *>(userData);

	me->states.checkExpiration(click_jiffies());
	
	timer->schedule_after_sec(TIMER_INTERVAL);
}

template <typename L4_HEADER> bool Firewall::process(Packet *p, const click_ip *iph, const L4_HEADER *l4h, PacketType type)
{
	FiveTuple key(iph, l4h);
	FirewallState *state = states.get(key);
	
	if (state)
	{
		if (type == PT_OUTBOUND)
			states.refresh(state);
		return true;
	}
	
	if (type == PT_OUTBOUND)
	{
		state = states.allocate(); assert(state);
		new(state) FirewallState(key);
		states.put(state);
		return true;
	}
	
	return false;
}

void Firewall::push(int port, Packet *p)
{
/* constant missing from ClickOS */
#ifdef CLICK_OS
	static const uint8_t IPPROTO_ICMP = 1;
#endif
	
	PacketType type = (PacketType)port;
	bool result;
	
	switch (p->ip_header()->ip_p)
	{
	case IPPROTO_TCP:
		result = process(p, p->ip_header(), p->tcp_header(), type);
		break;
		
	case IPPROTO_UDP:
		result = process(p, p->ip_header(), p->udp_header(), type);
		break;
		
	case IPPROTO_ICMP:
		result = process(p, p->ip_header(), reinterpret_cast<const click_icmp_sequenced *>(p->icmp_header()), type);
		break;
		
	default:
		result = false;
		break;
	}
	
	if (!result)
	{
		p->kill();
		return;
	}
	
	output(port).push(p);
}

CLICK_ENDDECLS

EXPORT_ELEMENT(Firewall)
