#include "lbmux.hh"
#include "../clickityclack/lib/checksumfixup.hh"
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/args.hh>

using namespace Change;

CLICK_DECLS

using namespace ClickityClack;

LBMux::LBMux()
	: timer(timerHook, this)
{
	memset(&iph, 0, sizeof(click_ip));
	
	iph.ip_v = 4;
	iph.ip_hl = sizeof(click_ip) >> 2;
	iph.ip_ttl = 250;
	iph.ip_p = IPPROTO_IPIP;
	
#if HAVE_FAST_CHECKSUM
	iph.ip_sum = ip_fast_csum((unsigned char *) &iph, sizeof(click_ip) >> 2);
#else
	iph.ip_sum = click_in_cksum((unsigned char *) &iph, sizeof(click_ip));
#endif
}

LBMux::~LBMux() {}

int LBMux::configure(Vector<String> &conf, ErrorHandler *errh)
{
	prealloc = 0;
	
	if (Args(conf, this, errh)
		.read("PREALLOC", IntArg(), prealloc)
		.complete() < 0)
	{
		return -1;
	}
	
	if (prealloc < 0)
		return errh->error("PREALLOC must be positive");
	
	return 0;
}

int LBMux::initialize(ErrorHandler *)
{
	timer.initialize(this);
	timer.schedule_after_sec(TIMER_INTERVAL);

	return 0;
}

void LBMux::timerHook(Timer *timer, void *userData)
{
	LBMux *me = static_cast<LBMux *>(userData);
	VIPTable::iterator it;

	for (it = me->vipTable.begin(); it != me->vipTable.end(); ++it)
		it.get()->second->stateTracker.checkExpiration(click_jiffies());
	
	timer->schedule_after_sec(TIMER_INTERVAL);
}

bool LBMux::VIP::addDIP(uint32_t ip)
{
	DIPTable::iterator dipIt = dips.find(ip);
	
	if (dipIt != dips.end())
		return false;
	
	DIP *dip = new DIP(ip); assert(dip);
	
	dips.set(ip, dip);
	dipQueue.push_front(dip);
	
	return true;
}

bool LBMux::VIP::removeDIP(uint32_t ip)
{
	DIPTable::iterator dipIt = dips.find(ip);
	
	if (dipIt == dips.end())
		return false;
	
	DIP *dip = dipIt.get()->second;
	
	while (!dip->mappings.empty())
		stateTracker.remove(dip->mappings.front());
	
	dips.erase(dipIt);
	dipQueue.erase(dip);
	delete dip;
	
	return true;
}

LBMux::Mapping::~Mapping()
{
	dip->mappings.erase(this);
}

static inline bool isConnectionInitiator(const click_tcp *tcpHeader)
{
	//const uint8_t SIGNIFICANT_FLAGS = TH_CWR | TH_ACK | TH_RST | TH_SYN | TH_FIN;
	const uint8_t SIGNIFICANT_FLAGS = TH_ACK | TH_RST | TH_SYN | TH_FIN;
	
	return (tcpHeader->th_flags & SIGNIFICANT_FLAGS) == TH_SYN;
}

Packet *LBMux::simple_action(Packet *p)
{
	uint32_t srcIP = p->ip_header()->ip_src.s_addr;
	uint16_t srcPort;
	uint8_t proto = p->ip_header()->ip_p;
	VIP *vip;
	DIP *dip;
	Endpoint endpoint;
	Mapping *mapping;
	
	switch (proto)
	{
	case IPPROTO_TCP:
		srcPort = p->tcp_header()->th_sport;
		break;
		
	case IPPROTO_UDP:
		srcPort = p->udp_header()->uh_sport;
		break;
		
	default:
		goto drop;
	}
	
	vip = vipTable.get(p->ip_header()->ip_dst.s_addr);
	if (!vip)
		goto drop;
	
	endpoint = Endpoint(srcIP, srcPort, proto);
	mapping = vip->stateTracker.get(endpoint);
	if (!mapping)
	{
		if (proto == IPPROTO_TCP && !isConnectionInitiator(p->tcp_header()))
			goto drop;
		
		dip = vip->allocDIP();
		if (!dip)
			goto drop;
		
		mapping = vip->stateTracker.allocate(); assert(mapping);
		new(mapping) Mapping(endpoint, dip);
		vip->stateTracker.put(mapping);
	}
	
	return encapsulate(p, vip->ip, mapping->dip->ip);
	
drop:
	p->kill();
	return NULL;
}

WritablePacket *LBMux::encapsulate(Packet *p, uint32_t vip, uint32_t dip)
{
	WritablePacket *wp = p->push(sizeof(click_ip));
	if (!wp)
		return 0;
	
	click_ip *ip = reinterpret_cast<click_ip *>(wp->data());
	memcpy(ip, &iph, sizeof(click_ip));
	
	ip->ip_src.s_addr = vip;
	ip->ip_dst.s_addr = dip;
	ip->ip_len = htons(wp->length());
	
	ip->ip_sum = checksumFold(
		checksumFixup32(0, vip,
		checksumFixup32(0, dip,
		checksumFixup16(0, ip->ip_len,
		ip->ip_sum))));
	
	wp->set_ip_header(ip, sizeof(click_ip));
	
	return wp;
}

enum
{
	H_LIST,
	
	H_ADD_VIP,
	H_ADD_DIP,
	
	H_DEL_VIP,
	H_DEL_DIP,
};

static void tokenize(const String &str, int startIndex, Vector<String> *vec)
{
	if (startIndex == str.length())
		return;
	
	int spaceIndex = str.find_left(' ', startIndex);
	
	if (spaceIndex == -1) /* no spaces */
	{
		vec->push_back(str.substring(startIndex, str.length() - startIndex));
	}
	else if (spaceIndex == startIndex) /* starts with space */
	{
		tokenize(str, startIndex + 1, vec);
	}
	else /* got some space */
	{
		vec->push_back(str.substring(startIndex, spaceIndex - startIndex));
		tokenize(str, spaceIndex + 1, vec);
	}
}

int LBMux::writeHandler(const String &conf, Element *e, void *thunk, ErrorHandler *errh)
{
	LBMux *me = (LBMux *)e;
	
	IPAddress vipAddr;
	IPAddress dipAddrStart;
	IPAddress dipAddrEnd;
	Vector<String> addresses;
	
	VIP *vip;
	VIPTable::iterator vipIt;
	
	bool success = false;
	
	switch ((intptr_t)thunk)
	{
	case H_ADD_VIP:
	case H_DEL_VIP:
		if (!IPAddressArg().parse(conf, vipAddr))
			return errh->error("bad IP address");
		break;
		
	case H_ADD_DIP:
	case H_DEL_DIP:
		tokenize(conf, 0, &addresses);
		if (addresses.size() != 2 && addresses.size() != 3)
			return errh->error("expected 2-3 arguments, got %d", addresses.size());
		
		if (!IPAddressArg().parse(addresses[0], vipAddr))
			return errh->error("bad VIP");
		if (!IPAddressArg().parse(addresses[1], dipAddrStart))
			return errh->error("bad DIP");
		if (addresses.size() == 3)
		{
			if (!IPAddressArg().parse(addresses[2], dipAddrEnd))
				return errh->error("bad end DIP");
			if (ntohl(dipAddrEnd.addr()) < ntohl(dipAddrStart.addr()))
				return errh->error("bad DIP range");
		}
		else
		{
			dipAddrEnd = dipAddrStart;
		}
			
		break;
		
	default:
		return errh->error("bad operation");
	}
	
	switch ((intptr_t)thunk)
	{
	case H_ADD_VIP:
		if (me->vipTable.find(vipAddr.addr()) != me->vipTable.end())
			return errh->error("VIP already exists");
		
		vip = new VIP(vipAddr.addr(), me->prealloc); assert(vip);
		me->vipTable.set(vipAddr.addr(), vip);
		break;
		
	case H_DEL_VIP:
		vipIt = me->vipTable.find(vipAddr.addr());
		if (vipIt == me->vipTable.end())
			return errh->error("VIP doesn't exist");
		
		vip = vipIt.get()->second;
		delete vip;
		me->vipTable.erase(vipIt);
		break;
		
	case H_ADD_DIP:
		vip = me->vipTable.get(vipAddr.addr());
		if (!vip)
			return errh->error("no such VIP");
		
		for (uint32_t i = ntohl(dipAddrStart.addr()); i <= ntohl(dipAddrEnd.addr()); i++)
			success = vip->addDIP(htonl(i)) || success;
		if (!success)
			return errh->error("DIP(s) already exist(s)");
		break;
		
	case H_DEL_DIP:
		vip = me->vipTable.get(vipAddr.addr());
		if (!vip)
			return errh->error("no such VIP");
		
		for (uint32_t i = ntohl(dipAddrStart.addr()); i <= ntohl(dipAddrEnd.addr()); i++)
			success = vip->removeDIP(htonl(i)) || success;
		if (!success)
			return errh->error("no such DIP");
		break;
		
	default:
		return errh->error("bad operation");
	}
	
	return 0;
}

String LBMux::readHandle(Element *e, void *thunk)
{
	LBMux *me = (LBMux *)e;
	String ret;
	VIPTable::iterator vipIt;
	DIPTable::iterator dipIt;
	
	switch ((intptr_t)thunk)
	{
	case H_LIST:
		for (vipIt = me->vipTable.begin(); vipIt != me->vipTable.end(); ++vipIt)
		{
			VIP *vip = vipIt.get()->second;
			
			ret += IPAddress(vip->ip).unparse() + "\n";
			
			for (dipIt = vip->dips.begin(); dipIt != vip->dips.end(); ++dipIt)
			{
				DIP *dip = dipIt.get()->second;
				
				ret += "\t" + IPAddress(dip->ip).unparse() + "\n";
			}
		}
		break;
		
	default:
		return "<error: bad operation>";
	}
	
	return ret;
}

void LBMux::add_handlers()
{
	add_read_handler( "list",    &readHandle,   H_LIST);
	
	add_write_handler("add_vip", &writeHandler, H_ADD_VIP);
	add_write_handler("add_dip", &writeHandler, H_ADD_DIP);
	
	add_write_handler("del_vip", &writeHandler, H_DEL_VIP);
	add_write_handler("del_dip", &writeHandler, H_DEL_DIP);
}

CLICK_ENDDECLS

EXPORT_ELEMENT(LBMux)

