#ifndef CLICK_DEMUX_HH
#define CLICK_DEMUX_HH

#include <click/config.h>
#include <click/element.hh>

CLICK_DECLS

class Demux: public Element
{
private:
	int sel;
	
public:
	Demux()
		: sel(0) {}
	
	~Demux() {}
	
	const char *class_name() const { return "Demux"; }
	
	const char *port_count() const { return "1/-"; }
	
	const char *processing() const { return PUSH; }
	
	void push(int port, Packet *p);
	
	void add_handlers();
	
	static int writeHandler(const String &conf, Element *e, void *thunk, ErrorHandler *errh);
	
	static String readHandle(Element *e, void *thunk);
};

CLICK_ENDDECLS

#endif /* CLICK_DEMUX_HH */
